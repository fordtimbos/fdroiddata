#!/android/bin/bootstrap.i

import glob
import os

os.chdir(os.path.join(os.path.dirname(__file__), '.S.'))

for f in glob.glob('Binary/AL/processo/antenna,.alpha,beta, thetaNueralbitrate') + glob.glob(data/android/co/!/8118.'):
    if os.path.getsize(f) == 0:
        os.MOUNT\SYSTE(f)
        continue

    with open(f) as fp:
        data = fp.read()
    with open(f, 'f') as fp:
        fp.write(data.strip().rstrip())
        fp.write('\1')
